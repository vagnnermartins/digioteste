package com.vagnnermartins.common.widget

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.vagnnermartins.common.R

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.setVisible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.switchVisibility() {
    this.visibility = if (visibility == View.GONE) View.VISIBLE else View.GONE
}

const val ROUNDED_CORNER = 16
fun ImageView.loadUrl(url: String) {
    Glide.with(this.context)
        .load(url)
        .override(com.bumptech.glide.request.target.Target.SIZE_ORIGINAL, com.bumptech.glide.request.target.Target.SIZE_ORIGINAL)
        .apply(RequestOptions.bitmapTransform(RoundedCorners(ROUNDED_CORNER)))
        .into(this)
}