package com.vagnnermartins.common.models

/**
 * Interface to map service models to app models.
 */
interface DataMapper<T, R> {

    fun mapFrom(it: T): R
}