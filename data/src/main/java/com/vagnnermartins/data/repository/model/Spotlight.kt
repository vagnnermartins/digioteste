package com.vagnnermartins.data.repository.model

data class Spotlight(val name: String, val image: String, val description: String)