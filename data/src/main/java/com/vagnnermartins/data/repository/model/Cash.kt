package com.vagnnermartins.data.repository.model

data class Cash(val title: String, val image: String, val description: String)