package com.vagnnermartins.data.repository.model

data class Product(val name: String, val image: String, val description: String)