package com.vagnnermartins.data.repository

import com.vagnnermartins.common.cache.RxCache
import com.vagnnermartins.data.mapper.DataMapperFactory
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.service.DigioService
import io.reactivex.Scheduler
import io.reactivex.Single

const val DIGIO_CACHE_KEY = "DIGIO_CACHE_KEY"

class DigioRepository(
    private val service: DigioService,
    private val cache: RxCache<Products>,
    private val dataMapperFactory: DataMapperFactory,
    private val ioScheduler: Scheduler,
    private val computationScheduler: Scheduler
) {

    /**
     * Gets the Products from the [cache] if there is a cached version and makes a network request
     * to get the products otherwise.
     *
     * @return [Single] that completes when successfully getting the products or errors if the campaign can't be retrieved.
     */
    fun getProducts(): Single<Products> {
        return cache.get(
            DIGIO_CACHE_KEY,
            service.getProducts().observeOn(computationScheduler).map { dataMapperFactory.productsMapper().mapFrom(it) },
            skipErrors = false
        )
            .firstOrError()
            .subscribeOn(ioScheduler)
    }
}