package com.vagnnermartins.data.repository.model

data class Products(val spotlights: List<Spotlight>, val products: List<Product>, val cash: Cash)