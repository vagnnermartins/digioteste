package com.vagnnermartins.data.mapper

import com.vagnnermartins.common.models.DataMapper
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.service.model.ProductsResponse

class ProductsMapper(private val dataMapperFactory: DataMapperFactory) :
    DataMapper<ProductsResponse, Products> {

    override fun mapFrom(it: ProductsResponse): Products =
        Products(
            it.spotlight.map { dataMapperFactory.spotlightMapper().mapFrom(it) },
            it.products.map { dataMapperFactory.productMapper().mapFrom(it) },
            dataMapperFactory.cashMapper().mapFrom(it.cash)
        )
}