package com.vagnnermartins.data.mapper

import com.vagnnermartins.common.models.DataMapper
import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.service.model.CashResponse

class CashMapper : DataMapper<CashResponse, Cash> {

    override fun mapFrom(it: CashResponse): Cash = Cash(it.title, it.image, it.description)
}