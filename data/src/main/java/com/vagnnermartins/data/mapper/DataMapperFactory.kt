package com.vagnnermartins.data.mapper

import com.vagnnermartins.common.models.DataMapper
import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.data.service.model.CashResponse
import com.vagnnermartins.data.service.model.ProductResponse
import com.vagnnermartins.data.service.model.ProductsResponse
import com.vagnnermartins.data.service.model.SpotlightResponse

class DataMapperFactory {

    fun productsMapper(): DataMapper<ProductsResponse, Products> = ProductsMapper(this)

    fun spotlightMapper(): DataMapper<SpotlightResponse, Spotlight> = SpotlightMapper()

    fun productMapper(): DataMapper<ProductResponse, Product> = ProductMapper()

    fun cashMapper(): DataMapper<CashResponse, Cash> = CashMapper()
}