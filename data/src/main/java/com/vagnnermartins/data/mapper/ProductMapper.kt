package com.vagnnermartins.data.mapper

import com.vagnnermartins.common.models.DataMapper
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.service.model.ProductResponse

class ProductMapper : DataMapper<ProductResponse, Product> {

    override fun mapFrom(it: ProductResponse): Product = Product(it.name, it.image, it.description)
}