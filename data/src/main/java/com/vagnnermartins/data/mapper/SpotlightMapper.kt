package com.vagnnermartins.data.mapper

import com.vagnnermartins.common.models.DataMapper
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.data.service.model.SpotlightResponse

class SpotlightMapper : DataMapper<SpotlightResponse, Spotlight> {

    override fun mapFrom(it: SpotlightResponse): Spotlight = Spotlight(it.name, it.image, it.description)
}