package com.vagnnermartins.data.service.model

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    val name: String,
    @SerializedName("imageURL") val image: String,
    val description: String
)