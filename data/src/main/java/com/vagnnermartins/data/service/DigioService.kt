package com.vagnnermartins.data.service

import com.vagnnermartins.data.service.model.ProductsResponse
import io.reactivex.Single
import retrofit2.http.GET

interface DigioService {

    @GET("products")
    fun getProducts() : Single<ProductsResponse>
}