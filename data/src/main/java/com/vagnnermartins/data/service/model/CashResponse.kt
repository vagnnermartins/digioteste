package com.vagnnermartins.data.service.model

import com.google.gson.annotations.SerializedName

data class CashResponse(
    val title: String,
    @SerializedName("bannerURL") val image: String,
    val description: String
)