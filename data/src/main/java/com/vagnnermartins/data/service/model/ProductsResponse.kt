package com.vagnnermartins.data.service.model

class ProductsResponse(val spotlight: List<SpotlightResponse>, val products: List<ProductResponse>, val cash: CashResponse)