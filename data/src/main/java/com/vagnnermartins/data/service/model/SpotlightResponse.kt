package com.vagnnermartins.data.service.model

import com.google.gson.annotations.SerializedName

data class SpotlightResponse(
    val name: String,
    @SerializedName("bannerURL") val image: String,
    val description: String
)