package com.vagnnermartins.data.mapper

import com.vagnnermartins.data.service.model.CashResponse
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Test

class CashMapperTest {

    private val dataMapper = CashMapper()

    @Test
    fun `Assert Cash Mapper`() {
        val title = "title"
        val image = "image"
        val description = "description"

        val gwCash = mockk<CashResponse> {
            every { this@mockk.title } returns title
            every { this@mockk.image } returns image
            every { this@mockk.description } returns description
        }

        val model = dataMapper.mapFrom(gwCash)

        assertEquals(title, model.title)
        assertEquals(image, model.image)
        assertEquals(description, model.description)
    }
}