package com.vagnnermartins.data.mapper

import com.vagnnermartins.data.service.model.SpotlightResponse
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Test

class SpotlightMapperTest {

    private val dataMapper = SpotlightMapper()

    @Test
    fun `Assert Product Mapper`() {
        val name = "name"
        val image = "image"
        val description = "description"

        val gwSpotlight = mockk<SpotlightResponse>() {
            every { this@mockk.name } returns name
            every { this@mockk.image } returns image
            every { this@mockk.description } returns description
        }

        val model = dataMapper.mapFrom(gwSpotlight)

        assertEquals(name, model.name)
        assertEquals(image, model.image)
        assertEquals(description, model.description)
    }
}