package com.vagnnermartins.data.mapper

import com.vagnnermartins.data.service.model.CashResponse
import com.vagnnermartins.data.service.model.ProductResponse
import com.vagnnermartins.data.service.model.ProductsResponse
import com.vagnnermartins.data.service.model.SpotlightResponse
import org.junit.Assert.assertEquals
import org.junit.Test

class ProductsMapperTest {

    private val dataMapperFactory = DataMapperFactory()
    private val dataMapper = ProductsMapper(dataMapperFactory)

    @Test
    fun `Assert Products Mapper`() {
        val spotlightName = "spotlightName"
        val spotlightImage = "spotlightImage"
        val spotlightDescription = "spotlightDescription"

        val productName = "productName"
        val productImage = "productImage"
        val productDescription = "productDescription"

        val cashTitle = "cashTitle"
        val cashImage = "cashImage"
        val cashDescription = "cashDescription"

        val spotlightResponse = SpotlightResponse(spotlightName, spotlightImage, spotlightDescription)
        val productResponse = ProductResponse(productName, productImage, productDescription)
        val cashResponse = CashResponse(cashTitle, cashImage, cashDescription)

        val productsResponse = ProductsResponse(listOf(spotlightResponse), listOf(productResponse), cashResponse)

        assertEquals(spotlightName, productsResponse.spotlight[0].name)
        assertEquals(spotlightImage, productsResponse.spotlight[0].image)
        assertEquals(spotlightDescription, productsResponse.spotlight[0].description)

        assertEquals(productName, productsResponse.products[0].name)
        assertEquals(productImage, productsResponse.products[0].image)
        assertEquals(productDescription, productsResponse.products[0].description)

        assertEquals(cashTitle, productsResponse.cash.title)
        assertEquals(cashImage, productsResponse.cash.image)
        assertEquals(cashDescription, productsResponse.cash.description)
    }
}