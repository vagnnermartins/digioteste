package com.vagnnermartins.data.repository

import com.vagnnermartins.common.cache.RxCache
import com.vagnnermartins.data.mapper.DataMapperFactory
import com.vagnnermartins.data.mapper.ProductsMapper
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.service.DigioService
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test

class DigioRepositoryTest {

    private val service = mockk<DigioService>(relaxed = true)
    private val cache = mockk<RxCache<Products>>(relaxed = true)
    private val dataMapperFactory = mockk<DataMapperFactory>(relaxed = true)
    private val productsMapper = mockk<ProductsMapper>(relaxed = true)

    private lateinit var repository: DigioRepository

    @Before
    fun setUp() {
        every { dataMapperFactory.productsMapper() } returns productsMapper

        repository = DigioRepository(service, cache, dataMapperFactory, Schedulers.trampoline(), Schedulers.trampoline())
    }

    @Test
    fun `Get Products attempts to get cached data`() {

        repository.getProducts().test()

        verify { cache.get(DIGIO_CACHE_KEY, any(), forceLoad = false, skipErrors = false) }
    }

    @Test
    fun `Get Produtcs uses service as data source`() {
        val sourceSlot = slot<Single<Products>>()

        every { cache.get(DIGIO_CACHE_KEY, capture(sourceSlot), forceLoad = false, skipErrors = false) } returns mockk(relaxed = true)

        repository.getProducts().test()

        verify { cache.get(DIGIO_CACHE_KEY, any(), forceLoad = false, skipErrors = false) }

        sourceSlot.captured.test()
        verify { service.getProducts() }
    }

    @Test
    fun `Get Products emits error when failing to get products`() {
        val exception = RuntimeException("Failed to get products")
        every { cache.get(DIGIO_CACHE_KEY, any(), forceLoad = false, skipErrors = false) } returns Flowable.error(exception)

        val testObserver = repository.getProducts().test()

        testObserver.assertError(exception)
    }
}