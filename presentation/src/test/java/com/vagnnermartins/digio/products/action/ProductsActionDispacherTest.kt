package com.vagnnermartins.digio.products.action

import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.products.ProductsHandler
import com.vagnnermartins.digio.products.action.ProductsAction.GoToCashDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.GoToProductDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.GoToSpotlightDetailScreen
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class ProductsActionDispacherTest {

    private val handler = mockk<ProductsHandler>(relaxed = true)
    private val dispatcher = ProductsActionDispatcher(handler)

    @Test
    fun `Go to spotlight detail is handled when go to spotlight action is dispatched`() {
        val spotlight = mockk<Spotlight>()

        dispatcher.dispatch(GoToSpotlightDetailScreen(spotlight))

        verify { handler.goToSpotlightDetailScreen(spotlight) }
    }

    @Test
    fun `Go to product detail is handled when go to product action is dispatched`() {
        val product = mockk<Product>()

        dispatcher.dispatch(GoToProductDetailScreen(product))

        verify { handler.goToProductDetailScreen(product) }
    }

    @Test
    fun `Go to cash detail is handled when go to cash action is dispatched`() {
        val cash = mockk<Cash>()

        dispatcher.dispatch(GoToCashDetailScreen(cash))

        verify { handler.goToCashDetailScreen(cash) }
    }
}