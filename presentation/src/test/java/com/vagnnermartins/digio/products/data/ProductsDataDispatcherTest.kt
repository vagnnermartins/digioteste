package com.vagnnermartins.digio.products.data

import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.digio.products.ProductsHandler
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class ProductsDataDispatcherTest {

    private val handler = mockk<ProductsHandler>(relaxed = true)
    private val dispatcher = ProductsDataDispatcher(handler)

    @Test
    fun `Bind Data is dispatched`() {
        val username = "username"
        val products = mockk<Products>()
        dispatcher.dispatch(ProductsData(username, products))

        verify { handler.bindData(username, products) }
    }
}