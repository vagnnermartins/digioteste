package com.vagnnermartins.digio.products

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.vagnnermartins.common.tests.TestObserver
import com.vagnnermartins.data.repository.DigioRepository
import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.products.action.ProductsAction
import com.vagnnermartins.digio.products.action.ProductsAction.GoToCashDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.GoToProductDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.GoToSpotlightDetailScreen
import com.vagnnermartins.digio.products.data.ProductsData
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductsViewModelTest {

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    private val repository = mockk<DigioRepository>(relaxed = true)

    private lateinit var viewModel: ProductsViewModel

    private val products = mockk<Products>()
    private val name = "name"

    @Before
    fun setUp() {
        every { repository.getProducts() } answers { Single.just(products) }

        viewModel = ProductsViewModel(name, repository, Schedulers.trampoline())
    }

    @Test
    fun `On init, products is fetched`() {
        val testObserver = TestObserver<ProductsData>()

        viewModel.data.observeForever(testObserver)

        verify { repository.getProducts() }
        assertEquals(ProductsData(name, products), testObserver.lastValue())
    }

    @Test
    fun `On spotlight detail clicked, go to spotlight detail is emitted`() {
        val spotlight = mockk<Spotlight>()
        val testObserver = TestObserver<ProductsAction>()

        viewModel.actions.observeForever(testObserver)

        viewModel.goToSpotlightClicked(spotlight)

        assertEquals(GoToSpotlightDetailScreen(spotlight), testObserver.lastValue())
    }

    @Test
    fun `On product detail clicked, go to product detail is emitted`() {
        val product = mockk<Product>()
        val testObserver = TestObserver<ProductsAction>()

        viewModel.actions.observeForever(testObserver)

        viewModel.goToProductClicked(product)

        assertEquals(GoToProductDetailScreen(product), testObserver.lastValue())
    }

    @Test
    fun `On cash detail clicked, go to cash detail is emitted`() {
        val cash = mockk<Cash>()
        val testObserver = TestObserver<ProductsAction>()

        viewModel.actions.observeForever(testObserver)

        viewModel.goToCashClicked(cash)

        assertEquals(GoToCashDetailScreen(cash), testObserver.lastValue())
    }
}