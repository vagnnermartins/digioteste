package com.vagnnermartins.digio.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.vagnnermartins.digio.products.ProductsActivity
import com.vagnnermartins.digio.R

const val SPLASH_DELAY = 1500L

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        init()
    }

    private fun init() {
        Handler().postDelayed({
            startActivity(ProductsActivity.newIntent(this))
        }, SPLASH_DELAY)
    }
}