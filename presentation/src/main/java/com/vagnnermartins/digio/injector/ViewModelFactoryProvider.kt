package com.vagnnermartins.digio.injector

import com.vagnnermartins.digio.products.ProductsViewModelFactory

object ViewModelFactoryProvider {

    fun provideProductsViewModelFactory(): ProductsViewModelFactory {
        // Mocking User name
        return ProductsViewModelFactory("Vagner Martins", Injector.provideDigioRepository())
    }
}