package com.vagnnermartins.digio.injector

import com.vagnnermartins.common.cache.MemoryCache
import com.vagnnermartins.common.cache.RxCache
import com.vagnnermartins.data.mapper.DataMapperFactory
import com.vagnnermartins.data.repository.DigioRepository
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.service.DigioService
import com.vagnnermartins.data.service.Gateway
import io.reactivex.schedulers.Schedulers

object Injector {

    private lateinit var cache: RxCache<Products>

    fun provideDigioRepository(): DigioRepository {
        if (!::cache.isInitialized) {
            cache = RxCache(MemoryCache())
        }
        return DigioRepository(provideDigioService(), cache, DataMapperFactory(), Schedulers.io(), Schedulers.computation())
    }

    private fun provideDigioService(): DigioService {
        return Gateway.build().create(DigioService::class.java)
    }
}