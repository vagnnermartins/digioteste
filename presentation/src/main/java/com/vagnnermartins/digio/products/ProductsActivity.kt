package com.vagnnermartins.digio.products

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.vagnnermartins.common.viewmodel.observe
import com.vagnnermartins.common.widget.hide
import com.vagnnermartins.common.widget.show
import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.R
import com.vagnnermartins.digio.injector.ViewModelFactoryProvider
import com.vagnnermartins.digio.products.action.ProductsActionDispatcher
import com.vagnnermartins.digio.products.data.ProductsDataDispatcher
import kotlinx.android.synthetic.main.activity_products.*

class ProductsActivity : AppCompatActivity(), ProductsHandler {

    private val viewModel by viewModels<ProductsViewModel> { ViewModelFactoryProvider.provideProductsViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)
        viewModel.actions.observe(this, ProductsActionDispatcher(this))
        viewModel.data.observe(this, ProductsDataDispatcher(this))

        error.setOnClickListener { viewModel.load() }
    }

    override fun goToSpotlightDetailScreen(spotlight: Spotlight) {
        Toast.makeText(this, "spotlight: ${spotlight.name}", Toast.LENGTH_LONG).show()
    }

    override fun goToProductDetailScreen(product: Product) {
        Toast.makeText(this, "product: ${product.name}", Toast.LENGTH_LONG).show()
    }

    override fun goToCashDetailScreen(cash: Cash) {
        Toast.makeText(this, "cash: ${cash.title}", Toast.LENGTH_LONG).show()
    }

    override fun bindData(username: String, data: Products) {
        progress.hide()
        error.hide()
        welcome.text = String.format(getString(R.string.welcome_message), username)
        spotlightView.setItems(data.spotlights) { viewModel.goToSpotlightClicked(it) }
        productView.setItems(data.products) { viewModel.goToProductClicked(it) }
        cashView.setItem(data.cash) { viewModel.goToCashClicked(it) }
    }

    override fun showProgress() {
        progress.show()
        error.hide()
    }

    override fun showError() {
        progress.hide()
        error.show()
    }

    companion object {

        fun newIntent(context: Context) = Intent(context, ProductsActivity::class.java)
    }
}