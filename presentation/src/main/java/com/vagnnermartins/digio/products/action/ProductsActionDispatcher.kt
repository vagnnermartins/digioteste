package com.vagnnermartins.digio.products.action

import com.vagnnermartins.common.viewmodel.Dispatcher
import com.vagnnermartins.digio.products.ProductsHandler

class ProductsActionDispatcher(private val handler: ProductsHandler) : Dispatcher<ProductsAction> {

    override fun dispatch(item: ProductsAction) = when (item) {
        is ProductsAction.GoToSpotlightDetailScreen -> handler.goToSpotlightDetailScreen(item.item)
        is ProductsAction.GoToProductDetailScreen -> handler.goToProductDetailScreen(item.item)
        is ProductsAction.GoToCashDetailScreen -> handler.goToCashDetailScreen(item.item)
        ProductsAction.ShowProgress -> handler.showProgress()
        ProductsAction.ShowError -> handler.showError()
    }
}