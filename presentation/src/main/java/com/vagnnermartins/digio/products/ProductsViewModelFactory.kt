package com.vagnnermartins.digio.products

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vagnnermartins.data.repository.DigioRepository
import io.reactivex.android.schedulers.AndroidSchedulers

class ProductsViewModelFactory(private val username: String, private val repository: DigioRepository) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProductsViewModel(username, repository, AndroidSchedulers.mainThread()) as T
    }
}