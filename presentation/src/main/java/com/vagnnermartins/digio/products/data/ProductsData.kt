package com.vagnnermartins.digio.products.data

import com.vagnnermartins.data.repository.model.Products

data class ProductsData(val username: String, val products: Products)