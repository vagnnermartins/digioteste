package com.vagnnermartins.digio.products

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.vagnnermartins.common.widget.loadUrl
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.digio.R

class ProductAdapter(
    private val items: List<Product>,
    private val listen: (Product) -> Unit
) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder =
        ProductViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val banner: ImageView = view.findViewById(R.id.image)

        fun bind(item: Product) {
            banner.loadUrl(item.image)
            itemView.setOnClickListener { listen.invoke(item) }
        }
    }
}