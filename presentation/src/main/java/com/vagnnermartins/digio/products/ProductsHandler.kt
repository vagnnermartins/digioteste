package com.vagnnermartins.digio.products

import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Products
import com.vagnnermartins.data.repository.model.Spotlight

interface ProductsHandler {

    fun goToSpotlightDetailScreen(spotlight: Spotlight)
    fun goToProductDetailScreen(product: Product)
    fun goToCashDetailScreen(cash: Cash)
    fun bindData(username: String, data: Products)
    fun showProgress()
    fun showError()
}