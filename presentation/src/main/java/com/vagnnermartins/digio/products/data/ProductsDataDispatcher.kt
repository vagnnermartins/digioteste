package com.vagnnermartins.digio.products.data

import com.vagnnermartins.common.viewmodel.Dispatcher
import com.vagnnermartins.digio.products.ProductsHandler

class ProductsDataDispatcher(private val handler: ProductsHandler) : Dispatcher<ProductsData> {

    override fun dispatch(item: ProductsData) = handler.bindData(item.username, item.products)
}