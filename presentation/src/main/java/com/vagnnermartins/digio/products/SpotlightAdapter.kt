package com.vagnnermartins.digio.products

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.vagnnermartins.common.widget.loadUrl
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.R

class SpotlightAdapter(
    private val items: List<Spotlight>,
    private val listen: (Spotlight) -> Unit
) : RecyclerView.Adapter<SpotlightAdapter.SpotlightViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotlightViewHolder =
        SpotlightViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_spotlight, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SpotlightViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class SpotlightViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val banner: ImageView = view.findViewById(R.id.image)

        fun bind(item: Spotlight) {
            banner.loadUrl(item.image)
            itemView.setOnClickListener { listen.invoke(item) }
        }
    }
}