package com.vagnnermartins.digio.products

import com.vagnnermartins.common.viewmodel.ActionViewModel
import com.vagnnermartins.data.repository.DigioRepository
import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.products.action.ProductsAction
import com.vagnnermartins.digio.products.action.ProductsAction.GoToCashDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.GoToProductDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.GoToSpotlightDetailScreen
import com.vagnnermartins.digio.products.action.ProductsAction.ShowError
import com.vagnnermartins.digio.products.action.ProductsAction.ShowProgress
import com.vagnnermartins.digio.products.data.ProductsData
import io.reactivex.Scheduler

class ProductsViewModel(
    private val username: String,
    private val repository: DigioRepository,
    private val uiScheduler: Scheduler
) : ActionViewModel<ProductsAction, ProductsData>() {

    init {
        load()
    }

    fun load() {
        disposeOnClear {
            dispatchAction(ShowProgress)
            repository.getProducts()
                .observeOn(uiScheduler)
                .subscribe({
                    //ProductData contém toda a informação a ser preenchida na View,
                    //porém poderiamos ter microserviços para Spotlight, Products e Cash e
                    //consequentemente ter um handler para cada informação.
                    dispatchData(ProductsData(username, it))
                }, {
                    dispatchAction(ShowError)
                })
        }
    }

    fun goToSpotlightClicked(item: Spotlight) = dispatchAction(GoToSpotlightDetailScreen(item))

    fun goToProductClicked(item: Product) = dispatchAction(GoToProductDetailScreen(item))

    fun goToCashClicked(item: Cash) = dispatchAction(GoToCashDetailScreen(item))
}