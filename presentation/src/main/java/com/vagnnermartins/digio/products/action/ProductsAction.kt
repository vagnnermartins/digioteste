package com.vagnnermartins.digio.products.action

import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Spotlight

sealed class ProductsAction {

    data class GoToSpotlightDetailScreen(val item: Spotlight) : ProductsAction()
    data class GoToProductDetailScreen(val item: Product) : ProductsAction()
    data class GoToCashDetailScreen(val item: Cash) : ProductsAction()
    object ShowProgress : ProductsAction()
    object ShowError : ProductsAction()
}