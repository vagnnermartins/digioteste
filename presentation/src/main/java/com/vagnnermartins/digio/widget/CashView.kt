package com.vagnnermartins.digio.widget

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.vagnnermartins.common.widget.loadUrl
import com.vagnnermartins.common.widget.show
import com.vagnnermartins.data.repository.model.Cash
import com.vagnnermartins.digio.R
import kotlinx.android.synthetic.main.view_cash.view.*

class CashView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_cash, this, true)
    }

    fun setItem(item: Cash, listen: (Cash) -> Unit) {
        val titleText = context.getString(R.string.digio_cash_section_title)
        Spannable.Factory.getInstance().newSpannable(titleText).apply {
            val lastWord = titleText.split(" ").last()
            setSpan(ForegroundColorSpan(Color.GRAY), titleText.indexOf(lastWord), titleText.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            title.text = this
        }
        image.loadUrl(item.image)
        setOnClickListener { listen.invoke(item) }
        show()
    }
}