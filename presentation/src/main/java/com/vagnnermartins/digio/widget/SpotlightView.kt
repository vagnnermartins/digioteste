package com.vagnnermartins.digio.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.vagnnermartins.common.widget.show
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.R
import com.vagnnermartins.digio.products.SpotlightAdapter
import kotlinx.android.synthetic.main.view_spotlight.view.*

class SpotlightView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_spotlight, this, true)
    }

    fun setItems(spotlights: List<Spotlight>, listen: (Spotlight) -> Unit) {
        items.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        items.adapter = SpotlightAdapter(spotlights, listen)
        show()
    }
}