package com.vagnnermartins.digio.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.vagnnermartins.common.widget.show
import com.vagnnermartins.data.repository.model.Product
import com.vagnnermartins.data.repository.model.Spotlight
import com.vagnnermartins.digio.R
import com.vagnnermartins.digio.products.ProductAdapter
import kotlinx.android.synthetic.main.view_products.view.*

class ProductView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_products, this, true)
    }

    fun setItems(products: List<Product>, listen: (Product) -> Unit) {
        items.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        items.adapter = ProductAdapter(products, listen)
        show()
    }
}